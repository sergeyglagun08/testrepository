<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%person}}`.
 */
class m191204_145953_create_person_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('person', [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer()->notnull(),
            'chief_id' => $this->integer(),
            'name' => $this->string(),
            'wage' => $this->float(),
            ]);

        $this->addForeignKey('person_branch', 'person', 'office_id', 'branch', 'id', 'CASCADE', 'CASCADE');

        $n=1;
        for($i=1; $i<=3; $i++){
            $this->insert('person', [
                'office_id' => $i,
                'chief_id' => null,
                'name' => $n++ . ' Number',
                'wage' => rand(1000.00,5000.00)/9+0.0001/2,
            ]);
            for($j=0; $j<4; $j++){
                $this->insert('person', [
                    'office_id' => $i,
                    'chief_id' => $i,
                    'name' => $n++ . ' Number',
                    'wage' => rand(1000.00,5000.00)/9+0.0001/2,
                ]);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%person}}');
    }
}

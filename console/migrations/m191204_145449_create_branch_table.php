<?php

use yii\db\Migration;

/**
 * m191204_145449_create_branch_table
 */
class m191204_145449_create_branch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('branch', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        $this->insert('branch', [
            'name' => 'First'
        ]);
        $this->insert('branch', [
            'name' => 'Second'
        ]);
        $this->insert('branch', [
            'name' => 'Third'
        ]);
        $this->insert('branch', [
            'name' => 'Fourth'
        ]);
        $this->insert('branch', [
            'name' => 'First'
        ]);
        $this->insert('branch', [
            'name' => 'Sixth'
        ]);
        $this->insert('branch', [
            'name' => 'Seventh'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('branch');
    }
}

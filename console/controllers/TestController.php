<?php
namespace console\controllers;

use yii\console\Controller;
use Yii;

class TestController extends Controller
{
    public function actionQueryOne()
    {
        $result = Yii::$app->db->createCommand(
        'SELECT person.chief_id,  person.name
            FROM person
            JOIN person AS p ON (person.chief_id = p.id)
            WHERE person.wage > p.wage'
        )->queryAll();

        var_dump($result);
    }

    public function actionQueryTwo()
    {
        $result = Yii::$app->db->createCommand(
            'SELECT b.id, b.name branch_name, p.wage, p.name person_name
            FROM branch b
            LEFT JOIN person p ON (p.office_id = b.id)
            WHERE p.wage = 
                (SELECT MAX(p2.wage) 
                FROM person p2 
                WHERE p2.office_id = b.id)'
        )->queryAll();

        var_dump($result);
    }
}